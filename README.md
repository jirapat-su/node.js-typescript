# Node.js TypeScript Template

This is a basic template for a Node.js project using TypeScript and Webpack.

## Prerequisites

Before you can run this application, you will need to have the following installed on your system:

- Node.js and npm or yarn
- Docker (optional)

## Installation

To install the dependencies, run:

```bash
npm install
```

## Development

To start the development server with hot reloading:

```bash
npm run dev
```

This will start the application using nodemon, which will automatically restart the application when changes are made.

## Building for Production

To build the production bundle and generate JavaScript files in the `dist` folder, run the following command:

```bash
npm run build
```

To start the production server:

```bash
npm run start
```

This will start the server using the JavaScript files in the `dist` folder.

## Linting

Run eslint to check for linting errors:

```bash
npm run lint
```

This will run the linting tool and output any errors found.

## Formatting

To automatically format the code using Prettier, run the following command:

```bash
npm run format
```

The "format" script uses Prettier to ensure consistently formatted and easy-to-read code, which can be useful for team projects.

## Build with Docker

To build the Docker image:

```bash
docker build -t node_app:latest .
```

To run the Docker container:

```bash
docker run --name node_app -d --restart unless-stopped -e PORT=1234 -p 1234:1234 node_app
```

## License

This project is licensed under the ISC License .
