# Build the application
FROM node:lts-alpine AS builder
WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build
RUN cd ./dist && npm install

# Production image
FROM node:lts-alpine AS runner

WORKDIR /app
COPY --from=builder /app/dist ./

ENV PORT=3001
ENV TZ=Asia/Bangkok

EXPOSE $PORT
CMD [ "npm", "start" ]
